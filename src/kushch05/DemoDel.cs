﻿using System;
using kushch01;
using kushch02;

namespace kushch05
{
    delegate bool CompareBy(Student student, string field);
    delegate void Avarage(LinkedList students, string field);

    class DemoDel
    {
        public CompareBy compare { get; set; }
        public Avarage avarage { get; set; }

        public void output(LinkedList students, string field)
        {
            if(compare != null)
            {
                string formatRow = "|{0,10}|{1,10}|{2,10}|{3,10}|{4,10}|{5,2}|{6,5}|{7,5}|{8,3}|";

                foreach(Student s in students)
                {
                    if(compare(s, field))
                    {
                        Console.WriteLine(String.Format(formatRow, s.Name, s.Surname, s.Patronymic, s.Birthday, s.AdmissionDate, s.IndexGroup, s.Faculty, s.Specialty, s.Progress));
                    }
                }
            } 
            else
            {
                Console.WriteLine("Choose Deligate before showing");
            }
        }

        public void delete(LinkedList students, string field)
        {
            if(compare != null)
            {
                for(int i = 0; i < students.size; i++)
                {
                    if (compare(students.GetI(i), field))       // GetI starts from 0
                    {
                        students.Remove(i + 1);                 // Remove starts from 1
                        i--;
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before deleteing");
            }
        }

        public void avarageAge(LinkedList students, string field)
        {
            if(compare != null)
            {
                float avarage = 0;

                foreach(Student s in students)
                {
                    if(compare(s, field))
                    {
                        avarage += DateTime.Now.Year - s.Birthday.Year;
                    }
                }

                avarage /= students.size;

                Console.WriteLine("Avarage Age: " + avarage);
            } 
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public void avarageProgress(LinkedList students, string field)
        {
            if(compare != null)
            {
                float avarage = 0;

                foreach (Student s in students)
                {
                    if (compare(s, field))
                    {
                        avarage += s.Progress;
                    }
                }

                avarage /= students.size;

                Console.WriteLine("Avarage Progress: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public bool CompareByGroup(Student student, string group)
        {
            return student.IndexGroup.Equals(group);
        }

        public bool CompareByFaculty(Student student, string faculty)
        {
            return student.Faculty.Equals(faculty);
        }
        public bool CompareBySpecialty(Student student, string specialty)
        {
            return student.Specialty.Equals(specialty);
        }
    }
}

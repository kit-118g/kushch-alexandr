﻿using System;
using System.IO;
using System.Xml.Serialization;
using kushch01;
using kushch02;

namespace kushch05
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList list = new LinkedList();
            DemoDel demo = new DemoDel();

            XmlSerializer xml = new XmlSerializer(typeof(LinkedList));

            int size = 0;
            int command = 0;
            int selectedDel = 0;
            string field;

            demo.avarage = demo.avarageAge;
            demo.avarage += demo.avarageProgress;

            while (command != 9)
            {
                Console.Write("1. Create Student\n2. ShowAll\n3. Show Student\n4. Delete\n5. Choose Deligate\n6. Avarage\n7. Serialize\n8. Deserialize\n9. Exit\nEnter number(1-9): ");
                command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        Console.Write("How many student: ");
                        size = int.Parse(Console.ReadLine());

                        for (int i = 0; i < size; i++)
                        {
                            list.Add(Controller.СreateStudent());
                        }

                        break;
                    case 2:
                        string formatRow = "|{0,10}|{1,10}|{2,10}|{3,10}|{4,10}|{5,2}|{6,5}|{7,5}|{8,3}|";

                        foreach (Student s in list)
                        {

                            Console.WriteLine(String.Format(formatRow, s.Name, s.Surname, s.Patronymic, s.Birthday, s.AdmissionDate, s.IndexGroup, s.Faculty, s.Specialty, s.Progress));
                        }

                        break;
                    case 3:
                        Console.WriteLine("Enter field by which you want see result: ");
                        field = Console.ReadLine();
                        demo.output(list, field);
                        break;
                    case 4:
                        Console.WriteLine("Enter field by which you want delete: ");
                        field = Console.ReadLine();
                        demo.delete(list, field);
                        break;
                    case 5:
                        Console.Write("1. By Group Index\n2. By Faculty\n3. By Specialty\n4. Cancel\nEnter deligate(1-3): ");
                        selectedDel = int.Parse(Console.ReadLine());
                        
                        switch(selectedDel)
                        {
                            case 1:
                                demo.compare = demo.CompareByGroup;
                                break;
                            case 2:
                                demo.compare = demo.CompareByFaculty;
                                break;
                            case 3:
                                demo.compare = demo.CompareBySpecialty;
                                break;
                        }

                        break;
                    case 6:
                        Console.Write("Enter field by which you want get avarage: ");
                        field = Console.ReadLine();

                        demo.avarage(list, field);
                        break;
                    case 7:
                        using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
                        {
                            xml.Serialize(fs, list);
                        }
                        break;
                    case 8:
                        using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
                        {
                            list = (LinkedList)xml.Deserialize(fs);
                        }
                        break;
                }
            }
        }
    }
}

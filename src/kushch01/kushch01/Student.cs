﻿using System;

namespace kushch01
{
    public class Student
    {
        public string Name {get; set;}
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime AdmissionDate { get; set; }
        public string IndexGroup { get; set; }
        public string Faculty { get; set; }
        public string Specialty { get; set; }
        public byte Progress { get; set; }
        public override string ToString()
        {
            return "Name: " + Name + "\nSurname: " + Surname + "\nPatronymic: " + Patronymic + "\nBirthday: "
                + Birthday + "\nAdmission: " + AdmissionDate + "\nIndex Group: " + IndexGroup + "\nFaculty: "
                + Faculty + "\nSpecialty: " + Specialty + "\nProgress: " + Progress + "\n\n";
        }
    }
}

﻿using System;
using System.Text.RegularExpressions;

namespace kushch01
{
    public class Controller
    {
        public static Student СreateStudent()
        {
            var student = new Student();

            Console.Write("Enter the name: ");
            student.Name = Controller.CheckString(Console.ReadLine());

            Console.Write("Enter the surname: ");
            student.Surname = Controller.CheckString(Console.ReadLine());

            Console.Write("Enter the patronymic: ");
            student.Patronymic = Controller.CheckString(Console.ReadLine());

            Console.Write("Enter your birthday(dd.mm.yyyy): ");
            student.Birthday = Controller.CheckDate(Console.ReadLine());

            Console.Write("Enter your admission date(dd.mm.yyyy): ");
            student.AdmissionDate = Controller.CheckAdmDate(Console.ReadLine(), student.Birthday);

            Console.Write("Enter your index of group(a-z): ");
            student.IndexGroup = Controller.CheckGrInd(Console.ReadLine());

            Console.Write("Enter your faculty: ");
            student.Faculty = Controller.CheckFC(Console.ReadLine());

            Console.Write("Enter your specialty: ");
            student.Specialty = Controller.CheckFC(Console.ReadLine());

            Console.Write("Enter the progress: ");
            student.Progress = Controller.CheckProgress(byte.Parse(Console.ReadLine()));

            return student;
        }

        public static void ShowAllStud(Student[] students)
        {
            for(int i = 0; i < students.Length; i++)
            {
                Console.WriteLine("Name: {0}\nSurname: {1}\nPatronymic: {2}\nBirthday: {3}\nAdmission date: {4}\nIndex Group: {5}\nFaculty: {6}\nSpecialty: {7}\nProgress: {8}\n\n",
                students[i].Name, students[i].Surname, students[i].Patronymic, students[i].Birthday, students[i].AdmissionDate, students[i].IndexGroup, students[i].Faculty, students[i].Specialty, students[i].Progress);
            }
        }

        public static string CheckString(string value)
        {
            string pattern = "[A-Z][a-z]+";

            while(!Regex.IsMatch(value, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                value = Console.ReadLine();
            }

            return value;
        }
        public static DateTime CheckDate(string dateStr)
        {
            DateTime dateRes = default;

            while(!DateTime.TryParse(dateStr, out dateRes))
            {
                Console.Write("Incorrect date, try again: ");
                dateStr = Console.ReadLine();
            }

            return dateRes;
        }

        public static DateTime CheckAdmDate(string admStr, DateTime birthday)
        {
            DateTime admDate = CheckDate(admStr);
            const int differenceBeetwenDates = 16;

            while (admDate < birthday.AddYears(differenceBeetwenDates))
            {
                Console.Write("Adm date must be greater then birthday, try again: ");
                admDate = CheckDate(Console.ReadLine());
            }

            return admDate;
        }

        public static string CheckGrInd(string index)
        {
            string pattern = "[a-z]+";
            
            while (!Regex.IsMatch(index, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                index = Console.ReadLine();
            }

            return index;
        }

        public static byte CheckProgress(byte progress)
        {
            while (progress > 100 && progress < 0)
            {
                Console.Write("Enter num in diaposone 0-100: ");
                progress = byte.Parse(Console.ReadLine());
            }

            return progress;
        }
        public static string CheckFC(string value)  // Check Specialty and Faculty.
        {
            string pattern = "[A-Z]+";

            while (!Regex.IsMatch(value, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                value = Console.ReadLine();
            }

            return value;
        }
    }
}

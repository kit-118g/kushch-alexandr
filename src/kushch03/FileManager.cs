﻿using System;
using System.IO;
using kushch01;
using kushch02;

namespace kushch03
{
    public class FileManager
    {
        private const string FileName = "students.txt";

        public void WriteStudents(LinkedList list) {
            StreamWriter streamWriter = new StreamWriter(FileName);
            streamWriter.Write(string.Empty);

            foreach(Student i in list)
            {
                streamWriter.WriteLine(i);
            }

            streamWriter.Close();
        }

        public LinkedList ReadStudents()
        {
            LinkedList list = new LinkedList();
            
            if(File.Exists(FileName))
            {
                StreamReader streamReader = new StreamReader(FileName);

                while (!streamReader.EndOfStream)
                {
                    Student student = new Student();

                    if ((student.Name = ParseLine(streamReader.ReadLine())) != "") {
                        student.Surname = ParseLine(streamReader.ReadLine());
                        student.Patronymic = ParseLine(streamReader.ReadLine());
                        student.Birthday = DateTime.Parse(ParseLine(streamReader.ReadLine()));
                        student.AdmissionDate = DateTime.Parse(ParseLine(streamReader.ReadLine()));
                        student.IndexGroup = ParseLine(streamReader.ReadLine());
                        student.Faculty = ParseLine(streamReader.ReadLine());
                        student.Specialty = ParseLine(streamReader.ReadLine());
                        student.Progress = Byte.Parse(ParseLine(streamReader.ReadLine()));

                        list.Add(student);
                    }
                }

                streamReader.Close();
                return list;
            }

            return list;
        }

        private string ParseLine(string fromFile)
        {
            if(fromFile != "")
            {
                string[] line = fromFile.Split(" ");
                
                return line[1];
            }
            else
            {
                return fromFile;
            }
        }

        public void RedactStudent(string newData, int index, int field)
        {
            LinkedList list = new LinkedList();

            list = ReadStudents();
            index--;

            switch(field)
            {
                case 1:
                    list.GetI(index).Name = newData;
                    break;
                case 2:
                    list.GetI(index).Surname = newData;
                    break;
                case 3:
                    list.GetI(index).Patronymic = newData;
                    break;
                case 4:
                    list.GetI(index).Birthday = DateTime.Parse(newData);
                    break;
                case 5:
                    list.GetI(index).AdmissionDate = DateTime.Parse(newData);
                    break;
                case 6:
                    list.GetI(index).IndexGroup = newData;
                    break;
                case 7:
                    list.GetI(index).Faculty = newData;
                    break;
                case 8:
                    list.GetI(index).Specialty = newData;
                    break;
                case 9:
                    list.GetI(index).Progress = Byte.Parse(newData);
                    break;

            }

            WriteStudents(list);
        }

        public void RemoveStudent(int index)
        {
            LinkedList list = new LinkedList();

            list = ReadStudents();

            list.Remove(index);

            WriteStudents(list);
        }
    }
}

﻿using System;
using kushch01;
using kushch02;

namespace kushch03
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList list = new LinkedList();
            FileManager file = new FileManager();
            LinkedList fileList = null;
            int size = 0;
            int num = 0;

            while(num != 5)
            {
                Console.Write("1. Create&Write Student\n2. Read Student\n3. Redact Student\n4. Delete Student\n5. Exit\nEnter number(1-5): ");
                num = int.Parse(Console.ReadLine());

                switch(num)
                {
                    case 1:
                        Console.Write("How many student: ");
                        size = int.Parse(Console.ReadLine());
                        for (int i = 0; i < size; i++)
                        {
                            list.Add(Controller.СreateStudent());
                        }

                        file.WriteStudents(list);
                        break;
                    case 2:
                        fileList = file.ReadStudents();

                        foreach (var i in fileList)
                        {
                            Console.WriteLine(i);
                        }
                        break;
                    case 3:
                        Console.Write("Enter which student for redact(1-{0}): ", fileList.size);
                        int index = int.Parse(Console.ReadLine());
                        Console.Write("1. Name\n2. Surname\n3. Patronymic\n4. Birthday\n5. Admission\n6. Index\n7. Faculty\n8. Specialty\n9. Progress\nEnter which field you want to redact: ");
                        int field = int.Parse(Console.ReadLine());
                        Console.Write("Enter new data: ");
                        string newData = Console.ReadLine();

                        file.RedactStudent(newData, index, field);
                        break;
                    case 4:
                        Console.Write("Enter which student for delete: ");
                        int numForDel = int.Parse(Console.ReadLine());

                        file.RemoveStudent(numForDel);
                        break;
                }
            }
        }
    }
}

﻿using kushch01;

namespace kushch04
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = Controller.СreateStudent();

            TreatmingString outputs = new TreatmingString();

            outputs.OutputGroup(student);
            outputs.OutputCourse(student.AdmissionDate);
            outputs.OutputAge(student.Birthday);
        }
    }
}

﻿using System;
using System.Text;
using kushch01;

namespace kushch04
{
    class TreatmingString
    {
        public void OutputGroup(Student student)
        {
            StringBuilder output = new StringBuilder("Faculty: {F}\nAdmission: {A}\nIndex: {I}");

            output.Replace("{F}", student.Faculty);
            output.Replace("{A}", student.AdmissionDate.ToString());
            output.Replace("{I}", student.IndexGroup);

            Console.WriteLine(output);
        }

        public void OutputCourse(DateTime admission)
        {
            StringBuilder output = new StringBuilder();
            DateTime now = DateTime.Now;

            int course = 0;
            int semester = 0;

            if(now.Month >= 9 && now.Month <= 12)
            {
                semester = 1;
            } 
            else
            {
                semester = 2;
            }

            if(semester == 1)
            {
                course = now.Year - admission.Year + 1;    // if admiss == Now => first course
            } 
            else
            {
                course = now.Year - admission.Year;
            }

            output.Append("Course: " + course);
            output.Append("\nSemester: " + semester);

            Console.WriteLine(output);
        }

        public void OutputAge(DateTime birthday)
        {
            DateTime now = DateTime.Now;
            int years = now.Year - birthday.Year;
            int days = DateTime.DaysInMonth(birthday.Year, birthday.Month) - birthday.Day + now.Day;
            int months = Math.Abs(birthday.Month - now.Month);
            if (birthday.Day >= now.Day)
            {
                months--;
            }

            StringBuilder output = new StringBuilder("Age:  years,  months,  days;");
            output.Insert(5, years);
            output.Insert(15, months);
            output.Insert(25, days);

            Console.WriteLine(output);
        }
    }
}

﻿using kushch01;

namespace kushch02
{
    public class Node
    {
        public Node(Student data)
        {
            Data = data;
        }

        public Student Data { get; set; }
        public Node Next { get; set; }
    }
}

﻿using System;
using kushch01;
using kushch02;
using System.Linq;

namespace kushch06
{
    class DemoLinq
    {
        public void OutputGroups(LinkedList students)
        {
            var names =
                from Student s in students
                select s.Name;

            var groups =
                from Student s in students
                select s.IndexGroup;

            foreach(var ng in names.Zip(groups, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,3}|", ng.Item1, ng.Item2);
            }
        }

        public void OutputCourses(LinkedList students)
        {
            var now = DateTime.Now;

            var names =
                from Student s in students
                select s.Name;

            var admissions =
                from Student s in students
                select now.Year - s.AdmissionDate.Year;

            foreach(var nc in names.Zip(admissions, Tuple.Create))
            {
                if(now.Month >= 9 && now.Month <= 12)
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2 + 1, 1);
                }
                else
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2, 2);
                }
            }
        }

        public void OutputAge(LinkedList students)
        {
            var now = DateTime.Now;

            var names =
                from Student s in students
                select s.Name;
            var years =
                from Student s in students
                select now.Year - s.Birthday.Year;

            foreach(var ny in names.Zip(years, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,10}|", ny.Item1, ny.Item2);
            }

        }
    }
}

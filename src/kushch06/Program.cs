﻿using kushch01;
using kushch02;
using System;

namespace kushch06
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList students = new LinkedList();
            DemoLinq demo = new DemoLinq();
            int size = 5;

            for (int i = 0; i < size; i++)
            {
                students.Add(Controller.СreateStudent());
            }

            demo.OutputGroups(students);
            Console.WriteLine();
            demo.OutputCourses(students);
            Console.WriteLine();
            demo.OutputAge(students);
        }
    }
}

﻿using kushch07.Classes;
using kushch07.Controllers;
using System;
using System.Linq;

namespace kushch07.UI
{
    class DemoDel
    {
        public void output(Controllers.LinkedList students, string field, CompareBy compare)
        {
            if (compare != null)
            {
                string formatRow = "|{0,10}|{1,10}|{2,10}|{3,10}|{4,10}|{5,2}|{6,5}|{7,5}|{8,3}|";

                foreach (Student s in students)
                {
                    if (compare(s, field))
                    {
                        Console.WriteLine(String.Format(formatRow, s.Name, s.Surname, s.Patronymic, s.Birthday, s.AdmissionDate, s.IndexGroup, s.Faculty, s.Specialty, s.Progress));
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before showing");
            }
        }

        public void delete(LinkedList students, string field, CompareBy compare)
        {
            if (compare != null)
            {
                for (int i = 0; i < students.size; i++)
                {
                    if (compare(students.GetI(i), field))       // GetI starts from 0
                    {
                        students.Remove(i + 1);                 // Remove starts from 1
                        i--;
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before deleteing");
            }
        }

        public void avarageAge(LinkedList students, string field, CompareBy compare)
        {
            if (compare != null)
            {
                var now = DateTime.Now;

                var avarage = (from Student s in students
                               where compare(s, field)
                              select now.Year - s.Birthday.Year).Average();

                Console.WriteLine("Avarage Age: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public void avarageProgress(LinkedList students, string field, CompareBy compare)
        {
            if (compare != null)
            {
                var avarage = (from Student s in students
                               where compare(s, field)
                               select (int)s.Progress).Average();

                Console.WriteLine("Avarage Progress: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

    }
}

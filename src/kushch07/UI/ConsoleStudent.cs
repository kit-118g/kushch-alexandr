﻿using System;
using kushch07.Classes;
using kushch07.Controllers;

namespace kushch07.UI
{
    class ConsoleStudent
    {
        public static Student СreateStudent()
        {
            var student = new Student();

            Console.Write("Enter the name: ");
            student.Name = StudentController.CheckString(Console.ReadLine());

            Console.Write("Enter the surname: ");
            student.Surname = StudentController.CheckString(Console.ReadLine());

            Console.Write("Enter the patronymic: ");
            student.Patronymic = StudentController.CheckString(Console.ReadLine());

            Console.Write("Enter your birthday(dd.mm.yyyy): ");
            student.Birthday = StudentController.CheckDate(Console.ReadLine());

            Console.Write("Enter your admission date(dd.mm.yyyy): ");
            student.AdmissionDate = StudentController.CheckAdmDate(Console.ReadLine(), student.Birthday);

            Console.Write("Enter your index of group(a-z): ");
            student.IndexGroup = StudentController.CheckGrInd(Console.ReadLine());

            Console.Write("Enter your faculty: ");
            student.Faculty = StudentController.CheckFC(Console.ReadLine());

            Console.Write("Enter your specialty: ");
            student.Specialty = StudentController.CheckFC(Console.ReadLine());

            Console.Write("Enter the progress: ");
            student.Progress = StudentController.CheckProgress(byte.Parse(Console.ReadLine()));

            return student;
        }

        public static void ShowAllStud(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine("Name: {0}\nSurname: {1}\nPatronymic: {2}\nBirthday: {3}\nAdmission date: {4}\nIndex Group: {5}\nFaculty: {6}\nSpecialty: {7}\nProgress: {8}\n\n",
                students[i].Name, students[i].Surname, students[i].Patronymic, students[i].Birthday, students[i].AdmissionDate, students[i].IndexGroup, students[i].Faculty, students[i].Specialty, students[i].Progress);
            }
        }

    }
}

﻿using kushch07.Classes;
using kushch07.Controllers;
using System;
using System.IO;
using System.Xml.Serialization;

namespace kushch07.UI
{
    class SwitchCaseUI
    {
        static LinkedList students = new LinkedList();
        static XmlSerializer xml = new XmlSerializer(typeof(LinkedList));

        static public void scList()
        {
            int command = 0;

            while (command != 6)
            {
                Console.Write("\t1. Create Students\n\t2. Delete Student\n\t3. Show All\n\t4. FindByName\n\t5. FindByIndex\n\t6. Cancle\n\tEnter number(1-6): ");
                command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        for (int i = 0; i < 5; i++)
                        {
                            students.Add(ConsoleStudent.СreateStudent());
                        }

                        break;
                    case 2:
                        Console.Write("\t\tWhich Student you want to remove(Since 1): ");
                        int remove = int.Parse(Console.ReadLine());

                        students.Remove(remove);
                        break;
                    case 3:
                        foreach (var s in students)
                        {
                            Console.WriteLine(s);
                        }
                        break;
                    case 4:
                        Console.Write("\t\tWhich name you want to find: ");
                        string name = Console.ReadLine();

                        LinkedList names = students.FindByName(name);
                        foreach (var n in names)
                        {
                            Console.WriteLine(n);
                        }
                        break;
                    case 5:
                        Console.Write("\t\tWhich index you want to find: ");
                        int index = int.Parse(Console.ReadLine());

                        Console.WriteLine(students.GetI(index));
                        break;
                }
            }
        }
        static public void scFile()
        {
            FileContoller file = new FileContoller();
            LinkedList fileList = null;
            int num = 0;

            while (num != 5)
            {
                Console.Write("\t1. Write Student\n\t2. Read Student\n\t3. Redact Student\n\t4. Delete Student\n\t5. Cancle\n\tEnter number(1-5): ");
                num = int.Parse(Console.ReadLine());

                switch (num)
                {
                    case 1:
                        file.WriteStudents(students);
                        break;
                    case 2:
                        fileList = file.ReadStudents();

                        foreach (var i in fileList)
                        {
                            Console.WriteLine(i);
                        }
                        break;
                    case 3:
                        Console.Write("\t\tEnter which student for redact(1-{0}): ", fileList.size);
                        int index = int.Parse(Console.ReadLine());
                        Console.Write("\t\t1. Name\n\t\t2. Surname\n\t\t3. Patronymic\n\t\t4. Birthday\n\t\t5. Admission\n\t\t6. Index\n\t\t7. Faculty\n\t\t8. Specialty\n" +
                            "\t\t9. Progress\n\t\tEnter which field you want to redact: ");
                        int field = int.Parse(Console.ReadLine());
                        Console.Write("\t\tEnter new data: ");
                        string newData = Console.ReadLine();

                        file.RedactStudent(newData, index, field);
                        break;
                    case 4:
                        Console.Write("Enter which student for delete: ");
                        int numForDel = int.Parse(Console.ReadLine());

                        file.RemoveStudent(numForDel);
                        break;
                }
            }
        }

        static public void scString()
        {
            Console.Write("Which student you want to get info(Since 0): ");
            int index = int.Parse(Console.ReadLine());

            Student student = students.GetI(index);
            DemoString demo = new DemoString();

            int command = 0;

            while (command != 4)
            {
                Console.Write("\t1. OutputGroup\n\t2. OutputCourse\n\t3. OutputAge\n\t4. Cancle\n\tEnter number(1-4): ");
                command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        demo.OutputGroup(student);

                        break;
                    case 2:
                        demo.OutputCourse(student.AdmissionDate);
                        break;
                    case 3:
                        demo.OutputAge(student.Birthday);
                        break;
                }
            }
        }

        static public void scDel()
        {
            Deligates del = new Deligates();
            DemoDel demo = new DemoDel();

            del.avarage = demo.avarageAge;
            del.avarage += demo.avarageProgress;

            int command = 0;
            int selectedDel = 0;
            string field;

            while (command != 5)
            {
                Console.Write("\t1. Choose Deligate\n\t2. Avarage\n\t3. OutputStud\n\t4.DeleteStud\n\t5. Canncle\n\tEnter number(1-5): ");
                command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        Console.Write("1. By Group Index\n2. By Faculty\n3. By Specialty\n4. Cancel\nEnter deligate(1-3): ");
                        selectedDel = int.Parse(Console.ReadLine());

                        switch (selectedDel)
                        {
                            case 1:
                                del.compare = del.CompareByGroup;
                                break;
                            case 2:
                                del.compare = del.CompareByFaculty;
                                break;
                            case 3:
                                del.compare = del.CompareBySpecialty;
                                break;
                        }

                        break;
                    case 2:
                        Console.Write("Enter field by which you want get avarage: ");
                        field = Console.ReadLine();

                        del.avarage(students, field, del.compare);
                        break;
                    case 3:
                        Console.WriteLine("Enter field by which you want see result: ");
                        field = Console.ReadLine();
                        demo.output(students, field, del.compare);
                        break;
                    case 4:
                        Console.WriteLine("Enter field by which you want delete: ");
                        field = Console.ReadLine();
                        demo.delete(students, field, del.compare);
                        break;
                }
            }
        }

        static public void scLinq()
        {
            DemoLinq demo = new DemoLinq();

            int command = 0;

            while (command != 4)
            {
                Console.Write("\t1. OutputGroup\n\t2. OutputCourse\n\t3. OutputAge\n\t4. Cancle\n\tEnter number(1-4): ");
                command = int.Parse(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        demo.OutputGroups(students);
                        break;
                    case 2:
                        demo.OutputCourses(students);
                        break;
                    case 3:
                        demo.OutputAges(students);
                        break;
                }
            }
        }

        static public void scSerialize()
        {
            using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
            {
                xml.Serialize(fs, students);
            }
        }

        static public void scDeserialize()
        {
            using (FileStream fs = new FileStream("students.xml", FileMode.OpenOrCreate))
            {
                students = (LinkedList)xml.Deserialize(fs);
            }
        }
    }
}

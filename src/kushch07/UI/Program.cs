﻿using kushch07.Classes;
using kushch07.Controllers;
using kushch07.UI;
using System;

namespace kushch07
{
    delegate bool CompareBy(Student student, string field);
    delegate void Avarage(LinkedList students, string field, CompareBy compare);

    class Program
    {
        static void Main(string[] args)
        {
            int command = 0;

            while(command != 9)
            {
                Console.Write("1. ListStudentsControl\n2. FileContoller\n3. StringBuilder\n4. DemoDeligate\n5. LINQ\n6.Serialize\n7.Deserialize\n9. Exit\nEnter number(1-6): ");
                command = int.Parse(Console.ReadLine());

                switch(command)
                {
                    case 1:
                        SwitchCaseUI.scList();
                        break;
                    case 2:
                        SwitchCaseUI.scFile();
                        break;
                    case 3:
                        SwitchCaseUI.scString();
                        break;
                    case 4:
                        SwitchCaseUI.scDel();
                        break;
                    case 5:
                        SwitchCaseUI.scLinq();
                        break;
                    case 6:
                        SwitchCaseUI.scSerialize();
                        break;
                    case 7:
                        SwitchCaseUI.scDeserialize();
                        break;
                }
            }

        }
    }
}

﻿using System;
using System.Text.RegularExpressions;

namespace kushch07.Controllers
{
    class StudentController
    {
        public static string CheckString(string value)
        {
            string pattern = "[A-Z][a-z]+";

            while (!Regex.IsMatch(value, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                value = Console.ReadLine();
            }

            return value;
        }
        public static DateTime CheckDate(string dateStr)
        {
            DateTime dateRes = default;

            while (!DateTime.TryParse(dateStr, out dateRes))
            {
                Console.Write("Incorrect date, try again: ");
                dateStr = Console.ReadLine();
            }

            return dateRes;
        }

        public static DateTime CheckAdmDate(string admStr, DateTime birthday)
        {
            DateTime admDate = CheckDate(admStr);
            const int differenceBeetwenDates = 16;

            while (admDate < birthday.AddYears(differenceBeetwenDates))
            {
                Console.Write("Adm date must be greater then birthday, try again: ");
                admDate = CheckDate(Console.ReadLine());
            }

            return admDate;
        }

        public static string CheckGrInd(string index)
        {
            string pattern = "[a-z]+";

            while (!Regex.IsMatch(index, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                index = Console.ReadLine();
            }

            return index;
        }

        public static byte CheckProgress(byte progress)
        {
            while (progress > 100 && progress < 0)
            {
                Console.Write("Enter num in diaposone 0-100: ");
                progress = byte.Parse(Console.ReadLine());
            }

            return progress;
        }
        public static string CheckFC(string value)  // Check Specialty and Faculty.
        {
            string pattern = "[A-Z]+";

            while (!Regex.IsMatch(value, pattern))
            {
                Console.Write("Incorrect data, try again: ");
                value = Console.ReadLine();
            }

            return value;
        }

    }
}

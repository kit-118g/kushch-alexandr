﻿using System;
using System.Collections;
using System.Xml.Serialization;
using kushch07.Classes;

namespace kushch07.Controllers
{
    [XmlInclude(typeof(Student))]
    [Serializable]
    public class LinkedList : IEnumerable
    {
        Node head;
        Node tail;
        public int size;

        public void Add(System.Object data)
        {
            Node node = new Node((Student)data);

            if (head == null)
            {
                head = node;
            }
            else
            {
                tail.Next = node;
            }
            tail = node;

            size++;
        }

        public void Remove(int index)
        {
            Node current = head;
            Node previous = null;

            int i = 1;

            while (i < index)
            {
                previous = current;
                current = current.Next;
                i++;
            }

            if (previous != null)
            {
                previous.Next = current.Next;

                if (current.Next == null)
                {
                    tail = previous;
                }
            }
            else
            {
                head = head.Next;

                if (head == null)
                {
                    tail = null;
                }
            }

            size--;
        }

        public LinkedList FindByName(String name)
        {
            Node current = head;
            LinkedList result = new LinkedList();

            while (current != null)
            {
                if (current.Data.Name == name)
                {
                    result.Add(current.Data);
                }

                current = current.Next;
            }

            return result;
        }

        public Student GetI(int index)
        {
            Node current = head;
            int i = 0;

            try
            {
                while (i < index && current != null)
                {
                    current = current.Next;
                    i++;
                }
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine($"{e.GetType().Name}: {index} is outside the bounds of the array");
            }

            return current.Data;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            Node current = head;

            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }
}

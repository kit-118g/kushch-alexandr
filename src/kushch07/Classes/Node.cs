﻿namespace kushch07.Classes
{
    class Node
    {
        public Node(Student data)
        {
            Data = data;
        }

        public Student Data { get; set; }
        public Node Next { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kushch07.Classes
{
    class Deligates
    {
        public CompareBy compare { get; set; }
        public Avarage avarage { get; set; }

        public bool CompareByGroup(Student student, string group)
        {
            return student.IndexGroup.Equals(group);
        }
        public bool CompareByFaculty(Student student, string faculty)
        {
            return student.Faculty.Equals(faculty);
        }
        public bool CompareBySpecialty(Student student, string specialty)
        {
            return student.Specialty.Equals(specialty);
        }

    }
}
